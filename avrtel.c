/* 
 * File:   avrtel.c
 * Author: InfoxElec
 *
 * Created on 3 avril 2020, 07:55
 * 
 * Interface for home phone with 8 stored phone numbres.
 * PD2 : Out C
 * PD3 : Out O
 * PD6 : Green LED
 * PD7 : red LED
 */
#define F_CPU 1000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include "telinterface.h"
#include "telkbdremote.h"

void CPU_init(void);
void read_config(void);
void checkKeyPressed(uint8_t port);

#define LEDV    PORTD6
#define LEDR    PORTD7

volatile uint8_t keypressed;
volatile uint8_t ptrnumber;

/*
 * 
 */
int main(void) {
    CPU_init();
    init_tel();
    init_kbd();
    //read_config();
    
    for(;;){
        //PORTB = PORTB ^ 1; //Keep alive LED on PORTB0
        //PORTD = PORTD ^ _BV(LEDV);
        PORTD ^= _BV(LEDV); // toggle green LED for heart beat
        /*Version clavier matriciel (telecommande)*/
        keypressed = readkey();
        if (keypressed != 0xFF){
            PORTD &= ~(_BV(LEDV));
            PORTD |= _BV(LEDR);
            if (keypressed >= 0 && keypressed <= 9){
                //On est dans les numeros.
                send_number(keypressed);
            }
            PORTD &= ~(_BV(LEDR));
        }
        //TODO : remplacer par un sleep mode
        _delay_ms(500);
    }
}

void CPU_init(){
    DDRB = 0x00;    //Set PORTB as input
    PORTB = 0xFF;   //Set pullup
    
    DDRD |= _BV(LEDV) | _BV(LEDR);    //Set PORTD6,7 as output
    PORTD &= ~(_BV(PORTD7)) | ~(_BV(PORTD6)); //Set ouput to 0
}

/* Read the configuration in EEPROM*/
void read_config(void){
    uint8_t addr = 0;
    addr += 1;
    //eeprom_read_byte(addr);
}