/*
 *  Tel interface file
 * All function too compose the phone number.
 *  
 */

#include <avr/io.h>
#include <util/delay.h>
#include "telinterface.h"

//TODO : save in EEPROM.
uint8_t numero[10][10] = {{0,6,2,9,6,9,0,8,4,8}, //1-Papa
                         {0,6,1,5,7,8,5,2,3,1}, //2-Maman
                         {0,6,2,9,3,2,1,6,3,8}, //3-Papy
                         {0,6,2,4,6,7,4,6,2,6}, //4-Mamie
                         {0,7,8,2,7,8,0,5,1,8}, //5-Tony
                         {0,6,1,0,4,9,7,0,1,0}, //6-Maraine
                         {0,6,9,9,0,5,8,6,1,6}, //7-Mémé
                         {0,6,9,9,3,1,6,1,6,5}, //8-Pépé
                         {0,6,5,1,1,3,1,4,1,0}, //9-Audrey L
                         {0,6,6,1,4,5,2,6,8,9}};//0-Christian G

/**
 * \fn void init_tel(void)
 * \brief Initialisation of the tel lines.
 * PORTtel 2 & 3 as ouput, set to 0.
 * \param void.
 * \return none.
 */
void init_tel(void){
    DDRtel |= _BV(outC) | _BV(outO);
    PORTtel &= ~(_BV(outC)) | ~(_BV(outO)); //Set ouput to 0
}

/**
 * \fn void send_digit(uint8_t digit)
 * \brief Send the digit to the tel line.
 * \param uint8_t digit : digit to be dialed
 * \return none.
 */
void send_digit(uint8_t digit){
    uint8_t tmpdigit;
    
    if(digit == 0){
        digit = 10;
    }
    PORTtel |= _BV(outC);
    _delay_ms(50); //initially 500
    
    tmpdigit = digit;
    do{
        _delay_ms(20);
        tmpdigit -= 1;
    } while (tmpdigit);
    
    if (digit > 1){
        do {
            _delay_ms(30);
            PORTtel |= _BV(outO); // active la sortie O
            _delay_ms(30);
            PORTtel &= ~(_BV(outO)); //desactive la sortie O
            digit -= 1;
        }while (digit > 1);
        _delay_ms(30);
    }
    PORTtel |= _BV(outO);
    _delay_ms(14);
    PORTtel &= ~(_BV(outC));
    _delay_ms(16);
    PORTtel &= ~(_BV(outO));
}

/**
 * \fn void send_number(uint8_t ptrnum)
 * \brief Compose the tel numbre.
 * \param uint8_t ptrnum : Pointer to the phone numbre to be dialed
 * \return none.
 */
void send_number(uint8_t ptrnum){
    uint8_t tmpdigit;

    for (uint8_t i = 0; i < 10; i++){
        tmpdigit = numero[ptrnum][i];
        send_digit(tmpdigit);
        _delay_ms(1000);
    }
}