/* 
 * File:   telinterface.h
 * Author: tux
 *
 * Created on 8 avril 2020, 12:44
 */

#define outC    PORTD2
#define outO    PORTD3
#define PORTtel PORTD
#define DDRtel  DDRD

void init_tel(void);
void send_digit(uint8_t digit);
void send_number(uint8_t ptrnum);
