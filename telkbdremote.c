/**
 * File:   avrtel.c
 * Author: InfoxElec
 *
 * Created on 3 avril 2020, 07:55
 * 
 * TV Remote controller keyboard based.
 * 
 */
 
#include <avr/io.h>
#include <util/delay.h>
#include "telkbdremote.h"

/**
 * \fn void init_kbd(void)
 * \brief Initialisation of the keyboard.
 * KBDPORT 6 & 7 as ouput, inputs with pullup.
 * \param void.
 * \return none.
 */
void init_kbd(void){
    KBDDDR = 0xC0; //KBDPORT6 and KBDPORT7 as output
    KBDPORT = 0xFF; //output set to 1 and input with pullup
}

/**
 * \fn uint8_t readkey(void)
 * \brief Read if keyboard key is selected
 * \param none.
 * \return uint8_t readkey : return the keypressed.
 */
uint8_t readkey(void){
    uint8_t kbdkey = 0xFF;
    uint8_t tmpport;
    
    //KBDPORT(6) = 0
    KBDPORT &= ~(_BV(6));
    _delay_ms(2);
    tmpport = KBDPIN;
    if ((tmpport & 0x3F) != 0x3F){
        // touche appuyee entre 1 et 6.
        uint8_t i;
        for (i = 0; i < 6; i++){
            if (!(tmpport & 1)){
                kbdkey = i;
            }
            tmpport = tmpport >> 1;
        }
    }
    KBDPORT |= _BV(6);
    
    if (kbdkey == 0xFF){
        KBDPORT &= ~(_BV(7));
        //_delay_ms(2);
        asm("nop");
        tmpport = KBDPIN;
        if ((tmpport & 0x3F) != 0x3F){
            uint8_t i;
            //touche appuyee entre 7 et ??
            for (i = 0; i < 6; i++){
                if (!(tmpport & 1)){
                    kbdkey = i + 6;
                }
                tmpport = tmpport >> 1;
            }            
        }
        KBDPORT |= _BV(7);
    }
    if (kbdkey == 9){
        //touche shift
        kbdkey = 0x10;
    }else if (kbdkey == 0x0A){
        //touche 0
        kbdkey = 9;
    }
    return kbdkey;    
}