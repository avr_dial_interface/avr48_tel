/* 
 * File:   telkbd.h
 * Author: tux
 *
 * Created on 13 avril 2020, 10:08
 */

#define KBDPORT PORTB
#define KBDDDR  DDRB
#define KBDPIN  PINB

void init_kbd(void);
uint8_t readkey(void);
